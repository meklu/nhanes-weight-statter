# NHANES weight stat tool

## What?

This tool processes NHANES survey data and plops out some nice numbers
pertaining to BMI and waist-to-height ratios.

To only process adult figures, records are filtered so that the field
`RIDAGEYR` (Age in years at screening) is greater than or equal to 18.

> Note: This has been revised after edition J came out: we used to use only the
> body measures data file and that had an applicable proxy in one of the
> variables. The variable in question was BMDBMIC (BMI Category -
> Children/Youth, only present for 2-19 year-olds), which happened to only be
> present for thus applicable records. This was present in edition I, but is
> unfortunately absent in edition J.
> The main difference is that now all subjects 18 years of age or older are
> counted rather than the previous 19+ year old age range.

### Editions? Huh?

The different editions of the NHANES surveys have an associated latin capital
letter. The two editions that this program has been tested against are edition
I (2015–2016) and edition J (2017–2018).

Currently, the only way to choose which edition to run the program against is
by editing the edition variable assignment in the `process.R` file. Changing
the value to 'I', for instance, would cause the program to be run against the
data from edition I.

## Preparation

This tool depends on the `make` utility and `R`.

From the provided web resource [1], obtain the Examination Data -> Body
Measures [2] and Demographics Data -> Demographic Variables and Sample Weights
[3] data files in the `XPT` format and place them in the directory this README
file resides in.

- [1] https://wwwn.cdc.gov/nchs/nhanes/continuousnhanes/default.aspx?BeginYear=2017
- [2] https://wwwn.cdc.gov/nchs/nhanes/search/datapage.aspx?Component=Examination&Cycle=2017-2018
- [3] https://wwwn.cdc.gov/nchs/nhanes/search/datapage.aspx?Component=Demographics&Cycle=2017-2018

## Usage

For your convenience, a `Makefile` is provided. To generate textual output,
simply run the following command in this directory:

```
make
```

If you do not have `make` installed, you can also run the following command:

```
R --slave -f ./process.R
```

Your terminal should be filled with a nice summary of the data.
